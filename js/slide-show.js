/*  
    global $ 
*/
    
//  Generic functions of global scope for utility on simple project:
    function createCarousel(){
        $('#myCarousel').carousel({ 
            interval: 5000
        })
    
    //  To view the next and previous slide:
        $('.carousel .item').each(function (e,i,e) {
            var next = $(this).next();
                !next.length ? next = $(this).siblings(':first') : null;
                next.children(':first-child').clone().appendTo($(this));
            
            var trueLength  = () => { return next.next().children(':first-child').clone().appendTo($(this)) };
            var falseLength = () => { return $(this).siblings(':first').children(':first-child').clone().appendTo($(this)) };
                next.next().length > 0 ? trueLength() : falseLength();
        });
        changeBackground(1);
    }

    function changeBackground(indexChild){
        $("#slideshow-background").css("background-image", "url('" + $(".active img")[indexChild].src + "')");
    }

    $(document).ready(function () {
        createCarousel()
        
    //  When the carousel goes to automatically (in the right):    
        $('#myCarousel').on('slide.bs.carousel', () => { changeBackground(2) });
    
    //  When the carousel goes to (right or left touch):    
        $("#slideshow-header").swiperight( () => { $("#myCarousel").carousel('prev'), changeBackground(0) });
        $("#slideshow-header").swipeleft(  () => { $("#myCarousel").carousel('next'), changeBackground(2) }); 
        
    });