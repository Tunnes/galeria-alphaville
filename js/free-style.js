var Picture = function(title, image, index){
    this.title = title;
    this.image = image;
    this.index = index;
    
    this.grid_item = document.createElement("div");
    this.grid_content = document.createElement("div");
    this.span_title = document.createElement("span");
    this.span_like  = document.createElement('span');
    this.span_view  = document.createElement("span");
}

Picture.prototype.addContent = function(){
    this.span_title.innerHTML = this.title;
    this.grid_content.style.backgroundImage = "url('" + this.image +"')";
    this.span_like.innerHTML = "<i class='fa fa-heart' aria-hidden='true'></i>"
    this.span_view.innerHTML = "<i class='fa fa-eye' aria-hidden='true'></i>";
}

Picture.prototype.addClasses = function(){
    var isBig = this.index == 5 ? "is-expanded " : "";
    var isExplaned = [1, 8, 11].indexOf(this.index) != -1 ? "grid-item-big " : "";
    
    this.grid_item.className = "grid-item " + isBig + isExplaned;
    this.grid_content.className = "grid-item-content " + isBig;
    this.span_like.className = "controls control-like";
    this.span_view.className ="controls control-view";
    this.span_title.className ="title";
}
Picture.prototype.build = function(){
    this.addContent();
    this.addClasses();
    this.grid_content.appendChild(this.span_title);
    this.grid_content.appendChild(this.span_like);
    this.grid_content.appendChild(this.span_view);
    this.grid_item.appendChild(this.grid_content);
    console.log(this.grid_item)
    document.getElementById('grid-wrapper').appendChild(this.grid_item);
}

function getEndpoint(){
    $.ajax({
        url: "https://jsonplaceholder.typicode.com/photos?_limit=20",
        statusCode: {
            200: function(data) {
                data = [].slice.call(data)
                data.map( (elemenet,index) => { (new Picture(elemenet.title, elemenet.url, index)).build() })
                makeGrids();
            }
        }
    });    
}

function makeGrids(){
    var grid = $('.grid').imagesLoaded( function() {
        grid = $('.grid').masonry({
            columnWidth: 120,
            itemSelector: '.grid-item'
        });
        
        grid.on( 'click', '.grid-item-content', function( event ) {
            $(".is-expanded:eq( 0 )").removeClass("is-expanded");
            $( event.currentTarget ).parent('.grid-item').toggleClass('is-expanded');
            grid.masonry();
        });
    });    
}

